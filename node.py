from network import Broadcast
import socket
import threading
from time import sleep

class Node:
    def __init__(self):
        self.broadcast = Broadcast()
        self.dispatcher = MessageDispatcher(self)
        self.name = socket.gethostbyname(socket.gethostname())
        self.start_neighbours_watcher()
        self.start_heartbeat()
        self.start_master_watcher()

    def start_master_watcher(self):
        self.master_watcher = MasterWatcher(self)
        thread = threading.Thread(target=self.master_watcher.run)
        thread.daemon = True
        thread.start()

    def start_neighbours_watcher(self):
        self.neighbours_watcher = NeighboursWatcher(self)
        thread = threading.Thread(target=self.neighbours_watcher.run)
        thread.daemon = True
        thread.start()

    def start_heartbeat(self):
        self.heartbeat = Heartbeat(self)
        thread =  threading.Thread(target=self.heartbeat.run)
        thread.daemon = True
        thread.start()

    def run(self):
        while True:
            message = self.broadcast.recv()
            self.dispatcher.dispatch(message)

class MasterWatcher:
    def __init__(self,node):
        self.node = node        

    def run(self):
        while True:
            sleep(5)

class NeighboursWatcher:
    def __init__(self,node):
        self.node = node

    def run(self):
        while True:
            try:
                for host in self.node.nodes:
                    self.node.nodes[host] = False
            except AttributeError:
                pass
            sleep(15)

    def active_nodes(self):
        return filter(lambda key: self.node.nodes[key],self.node.nodes.keys())

class MessageDispatcher:
    def __init__(self,node):
        self.node = node
        self.handlers = {   
                            'beat': self.heartbeat,
                            'who_master': self.who_master
                        }

    def dispatch(self,message):
        self.handlers[message[0]](message)

    def who_master(self,message):
        print(message)

    def heartbeat(self,message):
        try:
            self.node.nodes
        except AttributeError:
            self.node.nodes = {}
        self.node.nodes[ message[1][0] ] = True

class Heartbeat:
    def __init__(self,node):
        self.node = node

    def run(self):
        while True:
            self.node.broadcast.send("beat")
            sleep(5)
